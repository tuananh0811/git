package com.demo.entity.Entity;

import java.util.List;

public class Home {
    private List<Film> listTrending;
    private List<Film> listHot;
    private List<Film> listSuggest;
    private List<Film> listMatch;

    public List<Film> getListTrending() {
        return listTrending;
    }

    public void setListTrending(List<Film> listTrending) {
        this.listTrending = listTrending;
    }

    public List<Film> getListHot() {
        return listHot;
    }

    public void setListHot(List<Film> listHot) {
        this.listHot = listHot;
    }

    public List<Film> getListSuggest() {
        return listSuggest;
    }

    public void setListSuggest(List<Film> listSuggest) {
        this.listSuggest = listSuggest;
    }

    public List<Film> getListMatch() {
        return listMatch;
    }

    public void setListMatch(List<Film> listMatch) {
        this.listMatch = listMatch;
    }
}
